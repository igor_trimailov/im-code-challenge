import React, { FC } from "react";

import "./instructions.css";

export const Instructions: FC = () => {
  return (
    <div className="introduction">
      <h1>Intermedia Code Challenge</h1>
      <p>
        The image at the bottom of this page will look familiar to you as the
        design we asked you to decompose earlier. Your task is to implement the
        search and filter functionality shown in the design.
      </p>
        <p>
            We ask that you spend a maximum of 2 hours on this challenge.
            Once complete please push your branch, make sure you branch has your name included.
        </p>

      <h2>Allowances</h2>
      <p>
        There are some allowances made for the the sake of brevity and the lack
        of proper design hand off tools.
      </p>

      <h3>Storybook only</h3>
      <p>
        Your component only needs to run inside storybook. There is no app to
        run. A complete solution that runs in storybook is enough.
      </p>
      <p>
        <b>N.B</b> You can use the source of this page as a reference for
        creating stories if you&apos;re not familiar with storybook.
      </p>

      <h3>Pixel perfect design</h3>
      <p>
        As we can&apos;t expose the design in Figma you won&apos;t be able to
        inspect it to get pixel perfect values. As a result we are only looking
        for an accurate implementation of the UI, not pixel perfect. An SVG file
        has been provided for the right arrow shown when hovering a list item.
        All other components must be made by you.
      </p>

      <h3>Data layer</h3>
      <p>
        It is up to you to choose how you wish to implement the data layer. Keep
        in mind that we don&apos;t expect you to spend too long on this task. As
        this is being built solely inside storybook we will judge based on your
        implementation, not your technology choice. There is no need to fetch
        data from an API, hardcoded data is fine.
      </p>

      <h2>Requirements</h2>
      <ul>
        <li>Show all users on first load</li>
        <li>
          Filter the results based on the text input and filter button selection
        </li>
        <li>Only one filter button can be applied at a time</li>
        <li>
          User can click on the &apos;x&apos; in the input to clear the current
          search
        </li>
        <li>Only needs to work in chrome</li>
      </ul>

      <h2>Design</h2>
      <img src="search.png" className="image" />
    </div>
  );
};
