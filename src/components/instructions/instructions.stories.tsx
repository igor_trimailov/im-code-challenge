import React, { FC } from "react";
import { Instructions } from "./instructions";

export default {
  title: "Instructions",
};

export const main: FC = () => {
  return <Instructions />;
};
